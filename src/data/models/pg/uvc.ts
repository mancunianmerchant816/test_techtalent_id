import {
    Model,
    Table,
    Column,
    DataType,
    Sequelize,
    BelongsTo,
} from "sequelize-typescript";
import { EUvc } from "../../../domain";
import MVoucher from "./voucher";

@Table({
    tableName: "users",
    timestamps: false,
    underscored: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
})
export default class MUvc extends Model<EUvc> implements EUvc {
    @Column({
        allowNull: false,
        primaryKey: true,
        type: DataType.UUID,
        defaultValue: Sequelize.literal("gen_random_uuid()"),
    })
    id_uvc?: string | undefined;

    @Column({
        allowNull: false,
        type: DataType.UUID,
    })
    id_voucher?: string | undefined;

    @Column({
        allowNull: true,
        type: DataType.DATE,
        defaultValue: Sequelize.literal("now()"),
    })
    start_date?: Date | undefined;

    @Column({
        allowNull: true,
        type: DataType.DATE,
        defaultValue: Sequelize.literal("now()"),
    })
    end_date?: Date | undefined;

    @Column({
        allowNull: true,
        type: DataType.FLOAT()
    })
    amount?: number | undefined;

    @Column({
        allowNull: true,
        type: DataType.STRING()
    })
    code?: string | undefined;

    @Column({
        allowNull: false,
        type: DataType.DATE(6),
        defaultValue: Sequelize.literal("now()"),
    })
    created_at?: Date | undefined;

    @Column({
        allowNull: false,
        type: DataType.DATE(6),
        defaultValue: Sequelize.literal("now()"),
    })
    updated_at?: Date | undefined;

    @BelongsTo(() => MVoucher, { foreignKey: 'id_voucher' })
    voucher!: MVoucher
}