import {
    Model,
    Table,
    Column,
    DataType,
    Sequelize,
    HasMany,
} from "sequelize-typescript";
import { EVocuher } from "../../../domain";
import MUvc from "./uvc";

@Table({
    tableName: "AccessTokens",
    timestamps: false,
    underscored: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
})
export default class MVoucher extends Model<EVocuher> implements EVocuher {
    @Column({
        allowNull: false,
        primaryKey: true,
        type: DataType.UUID,
        defaultValue: Sequelize.literal("gen_random_uuid()"),
    })
    id_voucher?: string | undefined;

    @Column({
        allowNull: true,
        type: DataType.STRING(255)
    })
    name?: string | undefined;

    @Column({
        allowNull: true,
        type: DataType.DATE,
        defaultValue: Sequelize.literal("now()"),
    })
    start_date?: Date | undefined;

    @Column({
        allowNull: true,
        type: DataType.DATE,
        defaultValue: Sequelize.literal("now()"),
    })
    end_date?: Date | undefined;

    @Column({
        allowNull: true,
        type: DataType.FLOAT()
    })
    amount?: number | undefined;

    @Column({
        allowNull: false,
        type: DataType.DATE(6),
        defaultValue: Sequelize.literal("now()"),
    })
    created_at?: Date | undefined;

    @Column({
        allowNull: false,
        type: DataType.DATE(6),
        defaultValue: Sequelize.literal("now()"),
    })
    updated_at?: Date | undefined;
    
    @HasMany(() => MUvc, {foreignKey: 'id_voucher'})
    uvc!: MUvc
}