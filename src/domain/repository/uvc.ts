import { FindOptions } from "sequelize"
import MUvc from "../../data/models/pg/uvc"
import { EUvc } from "../entity"
import { IUvcRepo } from "../interfaces"

export class UvcRepo implements IUvcRepo {
    async create(data?: EUvc | undefined): Promise<MUvc> {
        const result = await MUvc.create(data)

        return result
    }

    async bulkCreate(data?: EUvc[] | undefined): Promise<MUvc> {
        const result = await MUvc.bulkCreate(data!, {
            logging: console.log,
        })

        return result[0]
    }
    async findOne(options?: FindOptions<any> | undefined): Promise<MUvc | null> {
        const result = await MUvc.findOne({
            ...options
        })

        return result ?? null
    }
    
    async update(data?: EUvc | undefined, options?: FindOptions<any> | undefined): Promise<any> {
        const result = await MUvc.update({
            ...data!,
        }, {
            where: options!.where!,
            logging: true
        })

        return result
    }
}