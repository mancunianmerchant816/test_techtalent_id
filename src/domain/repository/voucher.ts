import { FindOptions } from "sequelize"
import MVoucher from "../../data/models/pg/voucher"
import { EVocuher } from "../entity"
import { IVoucherRepo } from "../interfaces"

export class VoucherRepo implements IVoucherRepo {
    async create(data?: EVocuher | undefined): Promise<MVoucher> {
        const result = await MVoucher.create(data)

        return result
    }
    async bulkCreate(data?: EVocuher[] | undefined): Promise<MVoucher> {
        const result = await MVoucher.bulkCreate(data!, {
            logging: console.log,
            ignoreDuplicates: false
        })

        return result[0]
    }
    async findOne(options: FindOptions | undefined): Promise<MVoucher | null> {
        const result = await MVoucher.findOne({
            ...options,
            logging: true
        })

        return result ?? null
    }
    async update(data?: EVocuher | undefined, options?: FindOptions<any> | undefined): Promise<any> {
        const result = await MVoucher.update({
            ...data!,
        }, {
            where: options!.where!,
            logging: true
        })

        return result
    }
    
}