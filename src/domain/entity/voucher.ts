export type EVocuher = {
    id_voucher?: string
    name?: string
    start_date?: Date
    end_date?: Date
    amount?: number
    created_at?: Date
    updated_at?: Date
}