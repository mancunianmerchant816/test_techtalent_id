export type EUvc = {
    id_uvc?: string
    id_voucher?: string
    start_date?: Date
    end_date?: Date
    amount?: number
    code?: string
    created_at?: Date
    updated_at?: Date
}