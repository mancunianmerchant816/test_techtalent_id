import MVoucher from "../../../data/models/pg/voucher"
import { EVocuher } from "../../entity"

export interface ICreateVoucherUseCase {
  execute(data?: EVocuher): Promise<MVoucher>
}

export interface IEditVoucher {
  execute(data?: EVocuher): Promise<MVoucher>
}
