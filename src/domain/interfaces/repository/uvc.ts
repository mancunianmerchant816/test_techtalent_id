import { FindOptions } from "sequelize"
import MUvc from "../../../data/models/pg/uvc"
import { EUvc, ResponseDataE } from "../../entity"

export interface IUvcRepo {
    create(data?: EUvc): Promise<MUvc>
    bulkCreate(data?: EUvc[]): Promise<MUvc>
    findOne(options?: FindOptions): Promise<MUvc | null>
    findAll(options?: FindOptions): Promise<Pick<ResponseDataE<MUvc[]>, 'rows' | 'count'>>
    update(data?: EUvc, options?: FindOptions): Promise<any>
}