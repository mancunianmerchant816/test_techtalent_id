import { FindOptions } from "sequelize";
import MVoucher from "../../../data/models/pg/voucher";
import { EVocuher } from "../../entity";

export interface IVoucherRepo {
    create(data?: EVocuher): Promise<MVoucher>
    bulkCreate(data?: EVocuher[]): Promise<MVoucher>
    findOne(options?: FindOptions): Promise<MVoucher | null>
    update(data?: EVocuher, options?: FindOptions): Promise<any>
}