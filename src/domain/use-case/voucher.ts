import { AppErrorV1 } from "../../common"
import MVoucher from "../../data/models/pg/voucher"
import { EVocuher, ErrorE } from "../entity"
import { ICreateVoucherUseCase, IEditVoucher, IUvcRepo, IVoucherRepo } from "../interfaces"

export class CreateVocuherUseCase implements ICreateVoucherUseCase {
    constructor(private readonly repo: IVoucherRepo, private readonly repoUvc: IUvcRepo) { }

    async execute(data?: EVocuher | undefined): Promise<MVoucher> {
        const mapWhere: Map<string, any> = new Map<string, any>()
        data!.created_at = new Date()
        data!.updated_at = new Date()

        try {
            const res = await this.repo.create(data)

            return res
        } catch (error) {
            const e: ErrorE = error as ErrorE
            throw new AppErrorV1(
                e.statusCode ?? 400,
                false,
                `${e.message}`,
                '001'
            )
        }
    }
}

export class EditVoucherUseCase implements IEditVoucher {
    constructor(private readonly repo: IVoucherRepo, private readonly repoUvc: IUvcRepo) { }

    async execute(data: EVocuher | undefined): Promise<any> {
        // const mapData: Map<string, any> = new Map<string, any>()

        // mapData.set('where', {
        //     id: id
        // })

        // mapData.set('update', {
        //     ...data,
        //     updatedAt: new Date()
        // })

        // const checkExistEmp = await this.repo.findOne(mapData)

        // if (!checkExistEmp) {
        //     throw new Error(`Data id: ${id} tidak ada`)
        // }

        // const update = await this.repo.update(mapData)

        // return update
    }
}