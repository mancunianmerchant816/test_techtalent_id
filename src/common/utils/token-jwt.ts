import { IJwtWrapper, IToken } from '../interfaces'
import { UserE } from '../../domain'

export class TokenJwtUtil implements IToken {

  constructor(private readonly jwt: IJwtWrapper) {
  }

  async generateToken(
    data: any,
    expired: string | number | null
  ): Promise<string> {
    return this.jwt.sign(
      data,
      process.env.JWT_SECRET!,
      expired != null ? { expiresIn: expired } : {}
    )
  }

  async verifyToken(token: string): Promise<UserE> {
    const res = await this.jwt.verify(token, process.env.JWT_SECRET)
    return res
  }
}
