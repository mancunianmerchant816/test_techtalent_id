import { body, query } from "express-validator";

/**
 * Expected Request
 * 
 * {
 *   "name": "CuaMcacarsaree" // Not Null
 * }
 */
export default [
    body('name')
        .optional()
        .isString()
        .withMessage('name id must string'),
    body('amount')
        .optional()
        .isNumeric()
        .isInt()
        .withMessage('amount id must numeric'),
    body('start_date')
        .optional()
        .isDate()
        .withMessage('start_date id must date'),
    body('end_date')
        .optional()
        .isDate()
        .withMessage('end_date id must date'),
    query("page")
        .optional()
        .isNumeric(),
    query("per_page")
        .optional()
        .isNumeric(),
];