import { param } from "express-validator";

/**
 * Expected Request
 * 
 * {
 *   "name": "CuaMcacarsaree" // Not Null
 * }
 */
export default [
    param('id')
        .optional({ nullable: true })
        .isString()
        .withMessage('id must string')
];