import { UserE } from '../../domain'

export interface IToken {
  generateToken(data: any, expired: string | number | null): Promise<string>
  verifyToken(token: string): Promise<UserE>
}
