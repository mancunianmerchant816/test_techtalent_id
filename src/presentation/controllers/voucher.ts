import express from 'express'
import { BaseController } from "./base_controller";
import detailValidation from '../../common/validations/voucher/detail.validation';
import bodyValidation from '../../common/validations/voucher/body.validation';

const app = express.Router()

class UserController extends BaseController{
    public routes = (): express.Router => {
        app.route("/generate/uvc").post(bodyValidation, CreateUserHandler(new CreateUserUseCase(new UserRepo()), new AuthMiddleware(new TokenJwtUtil(jwt), new UserRepo())));
        app.route("/generate/uvc/:id").patch(detailValidation, bodyValidation, UpdateUserHandler(new UpdateUserUseCase(new UserRepo()), new AuthMiddleware(new TokenJwtUtil(jwt), new UserRepo())));

        return app;
    }
}

export default new UserController().routes()