import { validationResult } from "express-validator";
import RequestValidationException from "../../common/middlewares/request-validation";
import { NextFunction, Request } from "express";
import { AppErrorV1 } from "../../common";

export class BaseController {
  public requestValidator(request: Request, next: NextFunction) {

    const errors = validationResult(request);

    if (!errors.isEmpty()) {
      next(new AppErrorV1(
        `500`,
        false,
        `${JSON.stringify(errors.array())}`,
        '001'
      ))
    }
  }

}