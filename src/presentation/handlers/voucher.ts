import { NextFunction, Request, Response } from "express"
import { ErrorE, ICreateUserUseCase, IDeleteUserUseCase, IDetailUserUseCase, IListUserUseCase, ILoginUseCase, IUpdateUserUseCase, PaginationE, UserE } from "../../domain"
import { Api, AppErrorV1, AppLoggerV1, ManualPagination, TokenJwtUtil } from "../../common"
import { BaseController } from "../controllers/base_controller"
import { AuthMiddleware } from "../../common/middlewares/auth-middleware"

export function CreateUserHandler(useCase: ICreateUserUseCase, authMidlle: AuthMiddleware) {
    const handler = async (request: Request, reply: Response, next: NextFunction) => {
        new BaseController().requestValidator(request, next)
        authMidlle.checkIsAdmin("admin", request, reply, next)
        try {
            const created = await useCase.execute(request.body)

            AppLoggerV1.info('user', { created })
            return Api.created(request, reply, {
                success: true,
                code: '201',
                message: 'Data user berhasil di created',
                data: created
            })
        } catch (error) {
            const e: ErrorE = error as ErrorE
            AppLoggerV1.warn('user', { error })
            next(new AppErrorV1(
                e.statusCode ?? 400,
                false,
                `${e.message}`,
                '001'
            ))
        }
    }

    return handler
}

export function UpdateUserHandler(useCase: IUpdateUserUseCase, authMidlle: AuthMiddleware) {
    const handler = async (request: Request, reply: Response, next: NextFunction) => {
        new BaseController().requestValidator(request, next)
        authMidlle.checkIsAdmin("admin", request, reply, next)
        try {
            const updated = await useCase.execute(parseInt(request.params.id), request.body)

            AppLoggerV1.info('user', { updated })
            return Api.created(request, reply, {
                success: true,
                code: '200',
                message: 'Data user berhasil di updated'
            })
        } catch (error) {
            const e: ErrorE = error as ErrorE
            AppLoggerV1.warn('user', { error })
            next(new AppErrorV1(
                e.statusCode ?? `500`,
                false,
                `${e.message}`,
                '001'
            ))
        }
    }

    return handler
}