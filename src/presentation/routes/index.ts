import express from 'express'
import voucherController from '../controllers/voucher'

const route = express.Router()

route.use(voucherController)

export default route