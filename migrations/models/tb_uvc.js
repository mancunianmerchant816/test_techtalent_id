'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tb_uvc extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  tb_uvc.init({
    id_voucher: DataTypes.UUID,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE,
    amount: DataTypes.FLOAT,
    code: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'tb_uvc',
  });
  return tb_uvc;
};