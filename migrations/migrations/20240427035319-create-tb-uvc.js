'use strict'
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('tb_uvc', {
      id_uvc: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('gen_random_uuid()'),
      },
      id_voucher: {
        type: Sequelize.UUID
      },
      start_date: {
        type: Sequelize.DATE
      },
      end_date: {
        type: Sequelize.DATE
      },
      amount: {
        type: Sequelize.FLOAT
      },
      code: {
        type: Sequelize.STRING,
        unique: true
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }, {
      schema: "public",
      freezeTableName: true
    })

    await queryInterface.addIndex(
      "public.tb_uvc",
      ["id_voucher", "code"]
    )
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tb_uvc')
  }
}