'use strict'
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable(
      'tb_voucher',
      {
        id_voucher: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()'),
        },
        name: {
          type: Sequelize.STRING,
        },
        start_date: {
          type: Sequelize.DATE,
        },
        end_date: {
          type: Sequelize.DATE,
        },
        amount: {
          type: Sequelize.FLOAT,
        },
        created_at: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updated_at: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      },
      {
        schema: 'public',
        freezeTableName: true,
      }
    )

    await queryInterface.addIndex(
      "public.tb_voucher",
      ["name", "amount"]
    )
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tb_voucher')
  },
}
